"""
Preprocessing taken from: https://github.com/Abhishek-MLDL/logistic-sentiment/blob/master/Sentiment%20analysis%20with%20Logistic%20Regression.ipynb
"""
import re


def preprocessor(text):
    text = re.sub("<[^>]*>", "", text)
    emoticons = re.findall("(?::|;|=)(?:-)?(?:\)|\(|D|P)", text)
    # Remove any non-word character and append the emoticons,
    # removing the nose character for standarization. Convert to lower case
    text = (
        re.sub("[\W]+", " ", text.lower()) + " " + " ".join(emoticons).replace("-", "")
    )

    return text


def tokenizer(text):
    return text.split()


from nltk.stem import PorterStemmer

porter = PorterStemmer()


def tokenizer_porter(text):
    return [porter.stem(word) for word in text.split()]
