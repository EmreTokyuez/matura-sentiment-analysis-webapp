# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import pandas as pd
from preprocessing import preprocessor
from preprocessing import tokenizer
from preprocessing import tokenizer_porter


# %%
training = pd.read_csv("Dataset\imdb_train.csv", encoding="utf-8")
training.shape
training.dtypes
training.convert_dtypes().dtypes


# %%
training.shape
training.head(5)


# %%
testing = pd.read_csv("Dataset/imdb_test.csv", encoding="utf-8")
testing.head(5)
testing.shape
testing.dtypes
testing.convert_dtypes().dtypes


# %%
x_train = training.iloc[:, 0]
y_train = training.iloc[:, 1]
x_test = testing.iloc[:, 0]
y_test = testing.iloc[:, 1]


# %%


from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfVectorizer

tfidf = TfidfVectorizer()

param_grid = [
    {
        "vect__ngram_range": [(1, 1), (1, 2), (2, 2)],
        "vect__stop_words": ["english", None],
        "vect__tokenizer": [tokenizer, tokenizer_porter],
        "vect__preprocessor": [None, preprocessor],
        "vect__use_idf": [False],
        "vect__lowercase": [True],
    },
]
multi_tfidf = Pipeline([("vect", tfidf), ("clf", MultinomialNB())])
gs_multi_tfidf = GridSearchCV(
    multi_tfidf, param_grid, scoring="accuracy", cv=5, verbose=1, n_jobs=-1
)


# %%

gs_multi_tfidf.fit(x_train, y_train)


# %%
print(
    "Best parameter set for naive bayes without tfidf: "
    + str(gs_multi_tfidf.best_params_)
)
print("Best accuracy for naive bayes without tfidf: %.3f" % gs_multi_tfidf.best_score_)


# %%

gs_multi_tfidf.best_estimator_.score(x_test, y_test)


# %%
import joblib


joblib.dump(
    gs_multi_tfidf.best_estimator_,
    "Finished Models/MultinominalNB/Multinominalwoidf.joblib",
)


# %%

param_grid_idf = [
    {
        "vect__ngram_range": [(1, 1), (1, 2), (2, 2)],
        "vect__stop_words": ["english", None],
        "vect__tokenizer": [tokenizer, tokenizer_porter],
        "vect__preprocessor": [None, preprocessor],
        "vect__use_idf": [True],
    },
]
multi_tfidf_idf = Pipeline([("vect", tfidf), ("clf", MultinomialNB())])
gs_multi_tfidf_idf = GridSearchCV(
    multi_tfidf_idf, param_grid_idf, scoring="accuracy", cv=5, verbose=1, n_jobs=-1
)


# %%
gs_multi_tfidf_idf.fit(x_train, y_train)


# %%
print(
    "Best parameter set for naive bayes with tfidf: "
    + str(gs_multi_tfidf_idf.best_params_)
)
print("Best accuracy for naive bayes with tfidf: %.3f" % gs_multi_tfidf_idf.best_score_)


# %%
gs_multi_tfidf_idf.best_estimator_.score(x_test, y_test)


# %%
joblib.dump(
    gs_multi_tfidf_idf.best_estimator_,
    "Finished Models/MultinominalNB/Multinominalwithidf.joblib",
)

