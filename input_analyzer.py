import pickle
import joblib
from sklearn.pipeline import Pipeline
import nltk.sentiment.util
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import nltk
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from collections import Counter

"""
MultinominalNB_wo_tfidf_clf = pickle.load(
    open("Models\Finished Models\MultinominalNB\Multinominal_wo_TFidf", "rb")
)
MultinominalNB_wo_tfidf_vectorizer = pickle.load(
    open("Models\Finished Models\MultinominalNB\Multinominal_wo_Tfidf_vectorizer", "rb")
)

MultinominalNB_w_tfidf_clf = pickle.load(
    open("Models\Finished Models\MultinominalNB\Multinominal_w_TFidf", "rb")
)
MultinominalNB_w_tfidf_vectorizer = pickle.load(
    open("Models\Finished Models\MultinominalNB\Multinominal_w_Tfidf_vectorizer", "rb")
)
"""
# Multinominal_Grid = joblib.load("Models/Finished Models/MultinominalNB/MultinominalNB_Grid.pkl","r")
# Multinominal_Pipe = joblib.load("Models/Finished Models/MultinominalNB/MultinominalNB_pipeline.pkl","r")
import re
import nltk
from nltk.corpus import stopwords

stop = stopwords.words("english")

from sklearn.feature_extraction.text import TfidfVectorizer

tfidf = TfidfVectorizer(strip_accents=None)


def preprocessor(text):
    text = re.sub("<[^>]*>", "", text)
    emoticons = re.findall("(?::|;|=)(?:-)?(?:\)|\(|D|P)", text)
    # Remove any non-word character and append the emoticons,
    # removing the nose character for standarization. Convert to lower case
    text = (
        re.sub("[\W]+", " ", text.lower()) + " " + " ".join(emoticons).replace("-", "")
    )

    return text


from nltk.stem import PorterStemmer

porter = PorterStemmer()


def tokenizer(text):
    return text.split()


def tokenizer_porter(text):
    return [porter.stem(word) for word in text.split()]


Multinomial = joblib.load("Models/Finished Models/MultinominalNB/Multinominal.joblib")


sentence = input()


def demo_liu_hu_lexicon(sentence, plot=False):
    """
    Basic example of sentiment classification using Liu and Hu opinion lexicon.
    This function simply counts the number of positive, negative and neutral words
    in the sentence and classifies it depending on which polarity is more represented.
    Words that do not appear in the lexicon are considered as neutral.

    :param sentence: a sentence whose polarity has to be classified.
    :param plot: if True, plot a visual representation of the sentence polarity.
    """
    from nltk.corpus import opinion_lexicon
    from nltk.tokenize import treebank

    tokenizer = treebank.TreebankWordTokenizer()
    pos_words = 0
    neg_words = 0
    tokenized_sent = [word.lower() for word in tokenizer.tokenize(sentence)]

    x = list(range(len(tokenized_sent)))  # x axis for the plot
    y = []

    for word in tokenized_sent:
        if word in opinion_lexicon.positive():
            pos_words += 1
            y.append(1)  # positive
        elif word in opinion_lexicon.negative():
            neg_words += 1
            y.append(-1)  # negative
        else:
            y.append(0)  # neutral

    if pos_words > neg_words:
        return "Positive"
    elif pos_words < neg_words:
        return "Negative"
    elif pos_words == neg_words:
        return "Neutral"

    if plot == True:
        nltk._show_plot(
            x, y, x_labels=tokenized_sent, y_labels=["Negative", "Neutral", "Positive"]
        )


# Creating loop which runs the text into every method
while sentence != "exit":
    sid = SentimentIntensityAnalyzer()
    sid_result = sid.polarity_scores(sentence)
    print("SentimentIntensityAnalyzer:", sid_result)

    print("Lexicon-based approach:", demo_liu_hu_lexicon(sentence, plot=False))
    sentence_list = [sentence]
    """Sentence_Multinominal_wo_tfidf_vectorized = MultinominalNB_wo_tfidf_vectorizer.transform(
        sentence_list
    )"""
    """MultinominalNB_wo_tfidf_prediction = MultinominalNB_wo_tfidf_clf.predict(
        Sentence_Multinominal_wo_tfidf_vectorized
    )
    print("Multinominal without Tfidf:", MultinominalNB_wo_tfidf_prediction)

    Sentence_Multinominal_w_tfidf_vectorizer = MultinominalNB_w_tfidf_vectorizer.transform(
        sentence_list
    )
    MultinominalNB_w_tfidf_prediction = MultinominalNB_w_tfidf_clf.predict(
        Sentence_Multinominal_w_tfidf_vectorizer
    )
    print("Multinominal with Tfidf:", MultinominalNB_w_tfidf_prediction)
"""
    print("Multi says", Multinomial.predict(sentence_list))
    # sentence_new = Multinominal_Pipe.predict(sentence)
    # print("Grid says", Multinominal_Grid.predict(sentence_new))

    sentence = input()
