import json
import joblib
import nltk.sentiment.util
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import nltk
from flask import Flask, request, render_template
from preprocessing import preprocessor
from preprocessing import tokenizer
from preprocessing import tokenizer_porter
import os
from lstm2 import predict_sentiment
from cnn import predict_sentiment_cnn
from transformer import predict_sentiment_transformer

app = Flask(__name__)

MultinomialNB = joblib.load(
    "Models/Finished Models/MultinominalNB/Multinominalwoidf.joblib"
)
MultinomialNB_idf = joblib.load(
    "Models/Finished Models/MultinominalNB/Multinominalwithidf.joblib"
)
Logisticreg = joblib.load(
    "Models/Finished Models/LogisticRegression/LogisticRegressionwoidf.joblib"
)
Logisticreg_idf = joblib.load(
    "Models/Finished Models/LogisticRegression/LogisticRegressionwidf.joblib"
)


def demo_liu_hu_lexicon(sentence, plot=False):
    """
    Basic example of sentiment classification using Liu and Hu opinion lexicon.
    This function simply counts the number of positive, negative and neutral words
    in the sentence and classifies it depending on which polarity is more represented.
    Words that do not appear in the lexicon are considered as neutral.

    :param sentence: a sentence whose polarity has to be classified.
    :param plot: if True, plot a visual representation of the sentence polarity.
    """
    from nltk.corpus import opinion_lexicon
    from nltk.tokenize import treebank

    tokenizer = treebank.TreebankWordTokenizer()
    pos_words = 0
    neg_words = 0
    tokenized_sent = [word.lower() for word in tokenizer.tokenize(sentence)]

    x = list(range(len(tokenized_sent)))  # x axis for the plot
    y = []

    for word in tokenized_sent:
        if word in opinion_lexicon.positive():
            pos_words += 1
            y.append(1)  # positive
        elif word in opinion_lexicon.negative():
            neg_words += 1
            y.append(-1)  # negative
        else:
            y.append(0)  # neutral

    if pos_words > neg_words:
        return "Positive"
    elif pos_words < neg_words:
        return "Negative"
    elif pos_words == neg_words:
        return "Neutral"

    if plot == True:
        nltk._show_plot(
            x, y, x_labels=tokenized_sent, y_labels=["Negative", "Neutral", "Positive"]
        )


@app.route("/")
def home():
    return render_template("index.html")


@app.route("/predict", methods=["POST"])
def predict():
    text = request.form["text"]
    text_list = [text]
    lexicon_prediction = demo_liu_hu_lexicon(text, plot=False)
    naive_prediction = MultinomialNB.predict(text_list)
    naive_prediction_idf = MultinomialNB_idf.predict(text_list)
    logistic_prediction = Logisticreg.predict(text_list)
    logistic_prediction_idf = Logisticreg_idf.predict(text_list)
    lstm_prediction = predict_sentiment(sentence=text)
    cnn_prediction = predict_sentiment_cnn(sentence=text)
    transformer_prediction = predict_sentiment_transformer(sentence=text)
    return render_template(
        "index.html",
        lexicon_prediction=lexicon_prediction,
        naive_prediction=naive_prediction,
        naive_prediction_idf=naive_prediction_idf,
        logistic_prediction=logistic_prediction,
        logistic_prediction_idf=logistic_prediction_idf,
        lstm_prediction=lstm_prediction,
        cnn_prediction=cnn_prediction,
        transformer_prediction=transformer_prediction,
    )


@app.route("/predict_api", methods=["POST"])
def predict_api():
    """
    For direct API calls trought request
    """
    """
    data = request.get_json()
    data = json.dumps(data)
    data = [data]
    x = MultinominalNB_vectorizer.transform(data)
    tfidf = TfidfTransformer()
    x = tfidf.fit_transform(x)
    prediction = MultinominalNB_clf.predict(x)
    return render_template("index.html", prediction_text=prediction)
    """

